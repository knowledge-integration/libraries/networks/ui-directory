export default function permissionToEdit(stripes, record) {
  return(((stripes.hasPerm('ui-ill-directory.edit-all') ||
           stripes.hasPerm('ui-ill-directory.edit-local') ||
          (stripes.hasPerm('ui-ill-directory.edit-self') &&
           record?.status?.value === 'managed'))) &&
         (record?.canEdit !== 'No'));
}
