import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route, Switch } from 'react-router-dom';
import DirectoryEntries from './routes/DirectoryEntries';
import { useIntlKeyStore } from '@k-int/stripes-kint-components';

const Directory = (props) => {
  const {
    match: {
      path
    }
  } = props;
  const addKey = useIntlKeyStore(state => state.addKey);
  addKey('ui-ill-directory');

  return (
    <Switch>
      <Redirect
        exact
        from={path}
        to={`${path}/entries`}
      />
      <Route
        path={`${path}/entries`}
        render={() => <DirectoryEntries {...props} />}
      />
    </Switch>
  );
};

Directory.propTypes = {
  match: PropTypes.object.isRequired,
  actAs: PropTypes.string.isRequired,
  stripes: PropTypes.shape({
    connect: PropTypes.func,
  }),
};

export default Directory;
