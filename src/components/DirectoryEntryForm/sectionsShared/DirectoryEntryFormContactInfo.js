import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Field } from 'react-final-form';
import { FieldArray } from 'react-final-form-arrays';

import {
  Accordion,
  Col,
  Label,
  Row,
  TextField,
} from '@folio/stripes/components';

import { AddressListFieldArray } from '../components';

const DirectoryEntryFormContactInfo = ({
  id,
  onToggle,
  open,
}) => {
  return (
    <Accordion
      id={id}
      label={<FormattedMessage id="ui-ill-directory.information.heading.contactInformation" />}
      open={open}
      onToggle={onToggle}
    >
      <>
        <Row>
          <Col xs={4}>
            <Field
              id="edit-directory-entry-phone-number"
              name="phoneNumber"
              component={TextField}
              label={<FormattedMessage id="ui-ill-directory.information.mainPhoneNumber" />}
              parse={v => v}
            />
          </Col>
          <Col xs={4}>
            <Field
              id="edit-directory-entry-email-address"
              name="emailAddress"
              component={TextField}
              label={<FormattedMessage id="ui-ill-directory.information.mainEmailAddress" />}
              parse={v => v}
            />
          </Col>
          <Col xs={4}>
            <Field
              id="edit-directory-entry-contact-name"
              name="contactName"
              component={TextField}
              label={<FormattedMessage id="ui-ill-directory.information.mainContactName" />}
              parse={v => v}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={12}>
            <Label>
              <FormattedMessage id="ui-ill-directory.information.addresses" />
            </Label>
            <FieldArray
              name="addresses"
              component={AddressListFieldArray}
            />
          </Col>
        </Row>
      </>
    </Accordion>
  );
};

DirectoryEntryFormContactInfo.propTypes = {
  id: PropTypes.string.isRequired,
  onToggle: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired
};

export default DirectoryEntryFormContactInfo;
