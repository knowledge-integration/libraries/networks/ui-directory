import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { FieldArray } from 'react-final-form-arrays';

import {
  Accordion,
} from '@folio/stripes/components';

import { ServiceAccountListFieldArray } from '../components';
import { useServices } from '../../../hooks';

const DirectoryEntryFormServices = ({
  id,
  onToggle,
  open,
}) => {

  const servicesData = useServices();
  const services = servicesData?.map(service => ({ value: service.id, label: `${service.name} : ${service.address}` }));

  return (
    <Accordion
      id={id}
      label={<FormattedMessage id="ui-ill-directory.information.heading.services" />}
      open={open}
      onToggle={onToggle}
    >
      <FieldArray
        name="services"
        component={ServiceAccountListFieldArray}
        services={services}
      />
    </Accordion>
  );
};

DirectoryEntryFormServices.propTypes = {
  id: PropTypes.string,
  onToggle: PropTypes.func,
  open: PropTypes.bool,
};

export default DirectoryEntryFormServices;
