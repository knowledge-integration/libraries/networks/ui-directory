import { useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Field, useFormState } from 'react-final-form';
import { FieldArray } from 'react-final-form-arrays';

import {
  Accordion,
  Col,
  Label,
  MessageBanner,
  Row,
  Select,
  TextField,
} from '@folio/stripes/components';

import {
  useILLRefdata
} from '@k-int/stripes-ill';

import { SymbolListField } from '../components';
import { useNamingAuthorities } from '../../../hooks';

import { required } from '../../../util/validators';

const DirectoryEntryFormInfo = ({
  id,
  layer,
  onToggle,
  open,
  parentResources: {
    records: {
      records: directoryEntries = []
    } = {}
  } = {},
}) => {
  const [selectedParent, setSelectedParent] = useState('');
  const [warning, setWarning] = useState('');

  const { values } = useFormState();

  const typeValues = useILLRefdata('DirectoryEntry.Type');

  const namingAuthoritiesData = useNamingAuthorities();

  const directoryEntryValues = useMemo(() => {
    return [
      { value: '', label: '' },
      ...directoryEntries.map(obj => ({ value: obj.id, label: obj.fullyQualifiedName })).filter(obj => {
        if (values) {
          return obj.value !== values.id;
        } else {
          return obj.value;
        }
      }),
    ];
  }, [directoryEntries, values]);

  const namingAuthorities = useMemo(() => (
    namingAuthoritiesData?.map(obj => ({ value: obj.id, label: obj.symbol }))
  ), [namingAuthoritiesData]);

  return (
    <Accordion
      id={id}
      label={<FormattedMessage id="ui-ill-directory.information.heading.directoryEntry" />}
      open={open}
      onToggle={onToggle}
    >
      <>
        <Row>
          <Col xs={4}>
            <Field
              id="edit-directory-entry-name"
              name="name"
              label={<FormattedMessage id="ui-ill-directory.information.name" />}
              validate={required}
              required
            >
              {props => (
                <TextField
                  {...props}
                  onChange={(e) => {
                    props.input.onChange(e);
                    const inputValue = e.target.value;
                    let warningMessage = '';

                    if (inputValue != null && selectedParent.includes(inputValue)) {
                      warningMessage = <FormattedMessage id="ui-ill-directory.information.parent.warning" />;
                    }
                    setWarning(warningMessage);
                  }}
                  placeholder="Name"
                />
              )}

            </Field>
          </Col>
          <Col xs={4}>
            <FormattedMessage id="ui-ill-directory.information.type">
              {placeholder => (
                <Field
                  id="edit-directory-entry-type"
                  name="type"
                  label={placeholder[0]}
                  component={Select}
                  dataOptions={typeValues}
                  placeholder={placeholder[0]}
                />
              )}
            </FormattedMessage>
          </Col>
          <Col xs={4}>
            <FormattedMessage id="ui-ill-directory.information.slug">
              {placeholder => (
                <Field
                  id="edit-directory-entry-slug"
                  name="slug"
                  label={placeholder[0]}
                  component={TextField}
                  placeholder={placeholder[0]}
                  disabled={layer === 'edit'}
                  required
                  validate={required}
                />
              )}
            </FormattedMessage>
          </Col>
        </Row>
        <Row>
          {/* FIXME This field is permanently disabled??? Not sure what the point of having a select is here */}
          {values?.parent ?
            <Col xs={4}>
              <Field
                id="edit-directory-entry-parent"
                name="parent"
                label={<FormattedMessage id="ui-ill-directory.information.parent" />}
              >
                {props => (
                  <Select
                    {...props}
                    dataOptions={directoryEntryValues}
                    onChange={(e) => {
                      props.input.onChange(e);

                      // The below is finding the selected index and then grabbing the label of that element (saves doing a separate lookup)
                      const selectedValue = e.target[e.target.selectedIndex].text;
                      setSelectedParent(selectedParent);

                      let warningMessage = '';

                      if (values.name != null && selectedValue.includes(values.name)) {
                        warningMessage = <FormattedMessage id="ui-ill-directory.information.parent.warning" />;
                      }
                      setWarning(warningMessage)
                    }}
                    placeholder=" "
                    disabled
                  />
                )}
              </Field>
            </Col> : ''
          }
          <Col xs={4}>
            <Field
              id="edit-directory-entry-lms-location-code"
              name="lmsLocationCode"
              component={TextField}
              label={<FormattedMessage id="ui-ill-directory.information.lmsLocationCode" />}
              parse={v => v}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={12}>
            <Label>
              <FormattedMessage id="ui-ill-directory.information.symbols" />
            </Label>
            <FieldArray
              component={SymbolListField}
              name="symbols"
              namingAuthorities={namingAuthorities}
            />
          </Col>
        </Row>
        {warning ? (
          <MessageBanner type="warning">
            {' '}
            {warning}
            {' '}
          </MessageBanner>) : null
        }
      </>
    </Accordion>
  );
};

export default DirectoryEntryFormInfo;
