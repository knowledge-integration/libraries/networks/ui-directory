import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Field } from 'react-final-form';

import { useKiwtFieldArray } from '@k-int/stripes-kint-components';

import {
  Button,
  Select,
  TextField,
  Row,
  Col,
} from '@folio/stripes/components';

import { EditCard } from '@folio/stripes-erm-components';

import { required } from '../../../util/validators';

const SymbolListField = ({
  fields: { name },
  namingAuthorities
}) => {
  const { items, onAddField } = useKiwtFieldArray(name);

  const renderAddSymbol = () => {
    return (
      <Button
        id="add-symbol-btn"
        onClick={() => onAddField()}
      >
        <FormattedMessage id="ui-ill-directory.information.symbols.add" />
      </Button>
    );
  };

  return (
    <>
      {items?.map((_symbol, index) => {
        return (
          <EditCard
            header={<FormattedMessage id="ui-ill-directory.information.symbol.index" values={{ index }} />}
            key={`${name}[${index}].editCard`}
          >
            <Row>
              <Col md={4}>
                <Field
                  name={`symbols[${index}].authority`}
                  component={Select}
                  dataOptions={[{ value:'', label: '' }, ...namingAuthorities]}
                  label={<FormattedMessage id="ui-ill-directory.information.symbols.authority" />}
                  format={v => v?.id}
                  required
                  validate={required}
                />
              </Col>
              <Col md={4}>
                <Field
                  name={`${name}[${index}].symbol`}
                  label={<FormattedMessage id="ui-ill-directory.information.symbols.symbol" />}
                  component={TextField}
                  required
                  validate={required}
                />
              </Col>
              <Col md={4}>
                <Field
                  name={`${name}[${index}].priority`}
                  label={<FormattedMessage id="ui-ill-directory.information.symbols.priority" />}
                  component={TextField}
                />
              </Col>
            </Row>
          </EditCard>
        );
      })}
      {renderAddSymbol()}
    </>
  );
}

SymbolListField.propTypes = {
  namingAuthorities: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.string,
    label: PropTypes.string,
  })),
};

export default SymbolListField;
