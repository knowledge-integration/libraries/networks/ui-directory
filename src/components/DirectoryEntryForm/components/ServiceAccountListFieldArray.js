import PropTypes from 'prop-types';

import { FormattedMessage } from 'react-intl';
import { Field } from 'react-final-form';

import { useKiwtFieldArray } from '@k-int/stripes-kint-components';

import {
  Button,
  Col,
  Row,
  Select,
  TextField,
  TextArea,
} from '@folio/stripes/components';

import { EditCard } from '@folio/stripes-erm-components';
import { required } from '../../../util/validators';

const ServiceAccountListFieldArray = ({
  fields: { name },
  services,
}) => {
  const { items, onAddField, onDeleteField } = useKiwtFieldArray(name);

  const renderAddService = () => {
    return (
      <Button
        id="add-service-btn"
        onClick={() => onAddField()}
      >
        <FormattedMessage id="ui-ill-directory.information.services.add" />
      </Button>
    );
  };

  return (
    <>
      {items?.map((service, index) => {
        return (
          <EditCard
            header={<FormattedMessage id="ui-ill-directory.information.services.header" values={{ index }} />}
            key={`${name}[${index}].editCard`}
            onDelete={() => onDeleteField(index, service)}
            deleteButtonTooltipText={<FormattedMessage id="ui-ill-directory.information.services.deleteText" values={{ slug: service.slug ? service.slug : `[${index}]` }} />}
          >
            <Row>
              <Col xs={6}>
                <FormattedMessage id="ui-ill-directory.information.services.slug">
                  {placeholder => (
                    <Field
                      component={TextField}
                      id={`edit-directory-entry-service-[${index}]-slug`}
                      label={placeholder[0]}
                      name={`${name}[${index}].slug`}
                      placeholder={placeholder[0]}
                      required
                      validate={required}
                    />
                  )}
                </FormattedMessage>
              </Col>
              <Col xs={6}>
                <FormattedMessage id="ui-ill-directory.information.services.service">
                  {placeholder => (
                    <Field
                      component={Select}
                      dataOptions={services}
                      id={`edit-directory-entry-service-[${index}]-service`}
                      label={placeholder[0]}
                      name={`${name}[${index}].service`}
                      placeholder={placeholder[0]}
                      required
                      validate={required}
                    />
                  )}
                </FormattedMessage>
              </Col>
            </Row>
            <Row>
              <Col xs={6}>
                <FormattedMessage id="ui-ill-directory.information.services.accountDetails">
                  {placeholder => (
                    <Field
                      component={TextArea}
                      id={`edit-directory-entry-service-[${index}]-account-details`}
                      label={placeholder[0]}
                      name={`${name}[${index}].accountDetails`}
                      parse={v => v}
                      placeholder={placeholder[0]}
                    />
                  )}
                </FormattedMessage>
              </Col>
            </Row>
          </EditCard>
        );
      })}
      {renderAddService()}
    </>
  );
}

ServiceAccountListFieldArray.propTypes = {
  name: PropTypes.string,
  services: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired
  }))
};

export default ServiceAccountListFieldArray;
