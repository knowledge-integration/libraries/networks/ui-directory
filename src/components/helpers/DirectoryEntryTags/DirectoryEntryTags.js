// We use DirectoryEntryTags differently here than we do in ERM, they are solely internal DirectoryEntryTags
import {
  useCallback,
  useMemo,
} from 'react';

import PropTypes from 'prop-types';
import { useIntl } from 'react-intl';
import { useParams } from 'react-router-dom';
import { uniqBy, sortBy, isEqual } from 'lodash';

import { useOkapiKy } from '@folio/stripes/core';
import { TagsForm } from '@folio/stripes/smart-components';

import { Pane } from '@folio/stripes/components';

import {
  useDirectoryEntry,
  useDirectoryTags,
  useMutateDirectoryEntries
} from '../../../hooks';

const DirectoryEntryTags = ({
  onToggle,
}) => {
  const ky = useOkapiKy();
  const intl = useIntl();

  const { id } = useParams();

  // Get internal DirectoryEntryTags
  const { data: tags = [] } = useDirectoryTags();
  const tagValues = useMemo(() => tags.map(t => ({ value: t.value, label: t.normValue })), [tags]);

  // ENTITY GET/PUT
  const { data: directoryEntry } = useDirectoryEntry({ id });
  const entityTags = useMemo(() => (directoryEntry?.tags ?? []).map(tag => tag.value.toLowerCase()), [directoryEntry]);

  // A failed PUT does not cause a redraw of TagForm even if we change the state to ['clearing'] and back
  const {
    put: { mutateAsync: editDirectoryEntry, isLoading },
  } = useMutateDirectoryEntries({
    redirect: { put: false },
    returnQueryObject: { put: true }
  });

  // add tag to the list of entity tags
  // istanbul ignore next
  const saveEntityTags = useCallback((tagsToSave) => {
    const tagListMap = (directoryEntry?.tags ?? []).map(tag => ({ 'value': tag.value }));
    const tagsMap = tagsToSave.map(tag => ({ 'value': tag.value || tag }));

    const newTags = sortBy(uniqBy([...tagListMap, ...tagsMap], 'value'));

    editDirectoryEntry({
      id,
      tags: newTags
    });
  }, [directoryEntry, editDirectoryEntry]);

  const onRemove = useCallback((tag) => {
    const tagToDelete = (directoryEntry?.tags ?? []).filter(t => t.value.toLowerCase() === tag.toLowerCase());

    editDirectoryEntry({
      id,
      tags: [{ id: tagToDelete[0].id, _delete:true }]
    });
  }, [directoryEntry, editDirectoryEntry]);

  return (
    <Pane
      defaultWidth="20%"
      dismissible
      id="tags-helper-pane"
      onClose={onToggle}
      paneSub={intl.formatMessage(
        { id: 'stripes-ill.numberOfTags' },
        { count: entityTags.length }
      )}
      paneTitle={intl.formatMessage(
        { id: 'stripes-ill.tags' }
      )}
    >
      <TagsForm
        entityTags={entityTags}
        isUpdatingEntity={isLoading}
        onAdd={saveEntityTags}
        onRemove={onRemove}
        tags={tagValues}
      />
    </Pane>
  );
};

DirectoryEntryTags.propTypes = {
  link: PropTypes.string,
  onToggle: PropTypes.func
};

export default DirectoryEntryTags;
