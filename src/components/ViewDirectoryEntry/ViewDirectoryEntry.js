import {
  useCallback,
  useMemo,
  useState
} from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import get from 'lodash/get';

import {
  AccordionSet,
  Accordion,
  Button,
  Col,
  Icon,
  IconButton,
  Layer,
  Layout,
  LoadingPane,
  MessageBanner,
  Pane,
  PaneMenu,
  Row,
  ButtonGroup,
} from '@folio/stripes/components';

import { stripesConnect } from '@folio/stripes/core';
import { DeveloperJson } from '@k-int/ill-ui'; // WAT

import permissionToEdit from '../../util/permissionToEdit';
import EditDirectoryEntry from '../EditDirectoryEntry';

import {
  CustomProperties
} from './components';

import {
  DirectoryEntryInfo,
  ContactInformation,
  ServiceAccounts,
} from './sectionsShared';

const ViewDirectoryEntry = (props) => {
  // TODO we seem to pass entirety of props down later... not sure we should be doing that
  const {
    editLink,
    handleSubmit,
    history,
    mutator,
    onClose,
    onCloseEdit,
    onCreate,
    onEdit,
    paneWidth,
    parentResources,
    resources,
    stripes
  } = props;
  // TODO we can probs move these to zustand or something,
  // then instead of prop drilling open/onToggle we can pull
  // in from that global state instead
  const [sectionsShared, setSectionsShared] = useState({
    directoryEntryInfo: true,
    contactInformation: true,
    services: false,
    customProperties: false,
    developerInfo: false,
  });
  const [sectionsLocal, setSectionsLocal] = useState({
    directoryEntryInfo: true,
    contactInformation: true,
    services: false,
    customProperties: false,
    developerInfo: false,
  });

  // TODO feels like this could and perhaps should be in the URL...
  const [tab, setTab] = useState('shared');

  const record = useMemo(() => {
    return parentResources?.selectedRecord?.records?.[0] ?? {};
  }, [parentResources]);

  const recordIsPending = useMemo(() => {
    return parentResources?.selectedRecord?.isPending ?? true;
  }, [parentResources]);

  const handleToggleHelper = useCallback((helper) => {
    const currentHelper = resources.query?.helper;
    const nextHelper = currentHelper !== helper ? helper : null;
    mutator.query.update({ helper: nextHelper });
  }, [mutator, resources]);

  const handleToggleTags = useCallback(() => {
    handleToggleHelper('tags');
  }, [mutator, resources]);

  const handleSectionToggle = useCallback(({ id }) => {
    setSectionsLocal({
      ...sectionsLocal,
      [id]: !sectionsLocal[id],
    });

    setSectionsShared({
      ...sectionsShared,
      [id]: !sectionsShared[id],
    });
  }, [sectionsLocal, sectionsShared]);

  const sectionProps = useMemo(() => ({
    record,
    onToggle: handleSectionToggle,
    stripes: stripes,
  }), [handleSectionToggle, record, stripes]);

  const renderEditLayer = useCallback(() => {
    return (
      <FormattedMessage id="ui-ill-directory.editDirectoryEntry">
        {layerContentLabel => (
          <Layer
            isOpen={resources.query.layer === 'edit'}
            contentLabel={layerContentLabel}
          >
            <EditDirectoryEntry
              {...props}
              onCancel={onCloseEdit}
              onSubmit={handleSubmit}
              initialValues={record}
            />
          </Layer>
        )}
      </FormattedMessage>
    );
  }, [handleSubmit, onCloseEdit, props, record, resources]);

  const renderUnitLayer = useCallback(() => {
    return (
      <FormattedMessage id="ui-ill-directory.editDirectoryEntry">
        {layerContentLabel => (
          <Layer
            isOpen={resources.query.layer === 'unit'}
            contentLabel={layerContentLabel}
          >
            <EditDirectoryEntry
              {...props}
              onCancel={onCloseEdit}
              onSubmit={onCreate}
              initialValues={{ parent: record }}
            />
          </Layer>
        )}
      </FormattedMessage>
    );
  }, [onCloseEdit, onCreate, props, record, resources]);

  const switchLayer = useCallback((newLayer) => {
    mutator.query.replace({ layer: newLayer });
  }, [mutator]);

  const paneButtons = useCallback(() => {
    return (
      <PaneMenu>
        <FormattedMessage id="ui-ill-ui.view.showTags">
          {ariaLabel => (
            <IconButton
              ariaLabel={ariaLabel[0]} // FIXME deprecated prop
              icon="tag"
              badgeCount={parentResources?.selectedRecord.records?.[0]?.tags?.length || 0}
              onClick={() => handleToggleTags()}
            />
          )}
        </FormattedMessage>
      </PaneMenu>
    );
  }, [handleToggleTags, resources]);

  const getActionMenu = useCallback(({ onToggle }) => {
      // TODO permissionToEdit could be a hook and not need the record/stripes object maybe?
      // Not sure this is the best approach but so long as it works...
    const showEditButton = useMemo(() => permissionToEdit(stripes, record), [record, stripes]);
    const showCreateUnitButton = useMemo(() => stripes.hasPerm('ui-ill-directory.create'), [stripes]);

    if (!showEditButton && !showCreateUnitButton) {
      // Nothing to include in the menu, so don't make one at all
      return null;
    }

    return (
      <>
        {showEditButton ? (
          <Button
            buttonStyle="dropdownItem"
            href={editLink}
            id="clickable-edit-directoryentry"
            onClick={() => {
              onEdit();
              onToggle();
            }}
          >
            <Icon icon="edit">
              <FormattedMessage id="ui-ill-directory.edit" />
            </Icon>
          </Button>
        ) : null}
        {showCreateUnitButton ? (
          <Button
            buttonStyle="dropdownItem"
            id="clickable-create-new-unit-directoryentry"
            onClick={() => {
              switchLayer('unit');
              onToggle();
            }}
          >
            <Icon icon="plus-sign">
              <FormattedMessage id="ui-ill-directory.createUnit" />
            </Icon>
          </Button>
        ) : null}
      </>
    );
  }, []);

  // Not sure about all these needing to be memoised and stored like this but *shrug*
  const title = useMemo(() => {
    if (!record.name) {
      return (
        <FormattedMessage id="ui-ill-directory.directoryEntry.view.fallbackPaneTitle" />
      );
    }

    if (record.status) {
      return (
        <FormattedMessage
          id="ui-ill-directory.directoryEntry.view.paneTitleWithStatus"
          values={{
            name: record.name,
            status: record.status.value
          }}
        />
      );
    }

    return record.name;
  }, [record]);

  const directoryEntry = useMemo(() => (record.name || <FormattedMessage id="ui-ill-directory.information.titleNotFound" />), [record]);

  if (recordIsPending) {
    return <LoadingPane />;
  }

  return (
    <Pane
      id="pane-view-directoryentry"
      defaultWidth="44%"
      paneTitle={title}
      dismissible
      onClose={(e) => {
        onClose(e);
        //TODO Extra step to handle weirdness in stripes route stuff, not sure where that came from tbh
        history.push(`/ill-directory/entries${location.search}`);
      }}
      lastMenu={paneButtons()}
      actionMenu={(actionMenuProps) => getActionMenu(actionMenuProps)}
    >
      <Layout className="textCentered">
        <ButtonGroup>
          <Button
            onClick={() => setTab('shared')}
            buttonStyle={tab === 'shared' ? 'primary' : 'default'}
            id="clickable-nav-shared"
          >
            <FormattedMessage id="ui-ill-directory.information.tab.shared" />
          </Button>
          <Button
            onClick={() => setTab('local')}
            buttonStyle={tab === 'local' ? 'primary' : 'default'}
            id="clickable-nav-local"
          >
            <FormattedMessage id="ui-ill-directory.information.tab.local" />
          </Button>
        </ButtonGroup>
      </Layout>
      {tab === 'shared' &&
        <>
          <Row>
            <Col xs={12} lgOffset={1} lg={10}>
              <MessageBanner>
                <FormattedMessage id="ui-ill-directory.information.heading.display-text" values={{ directory_entry: directoryEntry }} />
              </MessageBanner>
            </Col>
          </Row>
          <AccordionSet accordionStatus={sectionsShared}>
            <DirectoryEntryInfo id="directoryEntryInfo" {...sectionProps} />
            <ContactInformation id="contactInformation" {...sectionProps} />
            <ServiceAccounts id="services" {...sectionProps} />
            <CustomProperties id="customProperties" {...{ defaultInternal: false, ...sectionProps }} />
            <Accordion
              onToggle={handleSectionToggle}
              open={sectionsShared.developerInfo}
              id="developerInfo"
              label={<FormattedMessage id="ui-ill-directory.information.heading.developer" />}
              displayWhenClosed={<FormattedMessage id="ui-ill-directory.information.heading.developer.help" />}
            >
              <DeveloperJson src={record} />
            </Accordion>
          </AccordionSet>
        </>
      }
      {tab === 'local' &&
        <>
          <Row>
            <Col xs={12} lgOffset={1} lg={10}>
              <MessageBanner>
                <FormattedMessage id="ui-ill-directory.information.local.heading.display-text" />
              </MessageBanner>
            </Col>
          </Row>
          <AccordionSet accordionStatus={sectionsLocal}>
            <CustomProperties id="customProperties" {...sectionProps} />
          </AccordionSet>
        </>
      }
      { renderEditLayer() }
      { renderUnitLayer() }
    </Pane>
  );
};

ViewDirectoryEntry.propTypes = {
  editLink: PropTypes.string,
  mutator: PropTypes.shape({
    query: PropTypes.shape({
      replace: PropTypes.func,
    }),
  }),
  onClose: PropTypes.func,
  onCloseEdit: PropTypes.func,
  onCreate: PropTypes.func,
  onEdit: PropTypes.func,
  paneWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  resources: PropTypes.shape({
    query: PropTypes.shape({
      layer: PropTypes.string,
    }),
    selectedRecord: PropTypes.shape({
      records: PropTypes.array,
    }),
  }),
  stripes: PropTypes.object,
};

ViewDirectoryEntry.manifest = Object.freeze({
  query: {},
});

export default stripesConnect(ViewDirectoryEntry);
