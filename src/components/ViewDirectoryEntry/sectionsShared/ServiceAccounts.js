import { useCallback } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import {
  Accordion,
  Card,
  Col,
  Headline,
  KeyValue,
  NoValue,
  Row
} from '@folio/stripes/components';

const ServiceAccounts = ({
  id,
  onToggle,
  open,
  record: {
    services = []
  } = {}
}) => {

  const renderService = useCallback((service, slug) => {
    const header = service.name ||
      <FormattedMessage id="ui-ill-directory.information.serviceForSlug" values={{ slug }} />;
  
    return (
      <Card
        headerStart={<Headline margin="none">{header}</Headline>}
        cardStyle="positive"
        roundedBorder
        marginBottom0
      >
        <Row>
          <Col xs={6}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-directory.information.serviceFunction" />}
              value={service.businessFunction?.value}
            />
          </Col>
          <Col xs={6}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-directory.information.serviceType" />}
              value={service.type?.value}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={12}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-directory.information.serviceAddress" />}
              value={<tt>{service.address}</tt>}
            />
          </Col>
        </Row>
      </Card>
    );
  }, [])

  const renderServiceAccount = useCallback((serviceAcct, index, count) => {
    // XXX I don't know what service.accountDetails is
    const ss = serviceAcct.service;
    const header = serviceAcct.slug ||
      <FormattedMessage id="ui-ill-directory.information.serviceAcctNofM" values={{ index, count }} />;
  
    return (
      <Card
        key={index}
        headerStart={<Headline margin="none">{header}</Headline>}
        roundedBorder
        marginBottom0
      >
        <Row>
          <Col xs={6}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-directory.information.services.service" />}
              value={ss ? renderService(ss, serviceAcct.slug) : <NoValue />}
            />
          </Col>
          <Col xs={6}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-directory.information.services.accountDetails" />}
              value={serviceAcct.accountDetails}
            />
          </Col>
        </Row>
      </Card>
    );
  }, []);

  return (
    <Accordion
      id={id}
      label={<FormattedMessage id="ui-ill-directory.information.heading.services" />}
      open={open}
      onToggle={onToggle}
    >
      {services.map((service, i) => renderServiceAccount(service, i + 1, services.length))}
    </Accordion>
  );
}

ServiceAccounts.propTypes = {
  record: PropTypes.object,
  id: PropTypes.string,
  onToggle: PropTypes.func,
  open: PropTypes.bool,
};

export default ServiceAccounts;
