import { useMemo } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import orderBy from 'lodash/orderBy';

import { Accordion, Col, KeyValue, NoValue, Row } from '@folio/stripes/components';

import { Address } from '../components';

const ContactInformation = ({
  id,
  onToggle,
  open,
  record: {
    addresses = [],
    contactName,
    emailAddress,
    phoneNumber
  } = {}
}) => {

  const addressArray = useMemo(() => {
    const sortedAddresses = orderBy(addresses, ['addressLabel']);
    return sortedAddresses.map((address, i) => (
      <Address
        address={address}
        count={addresses.length}
        index={i + 1}
        key={`Address[${i + 1}], ${address?.addressLabel}`}
      />
    ));
  }, [addresses]);

  return (
    <Accordion
      id={id}
      label={<FormattedMessage id="ui-ill-directory.information.heading.contactInformation" />}
      open={open}
      onToggle={onToggle}
    >
      <Row>
        <Col xs={4}>
          <KeyValue
            label={<FormattedMessage id="ui-ill-directory.information.mainPhoneNumber" />}
            value={phoneNumber ?? <NoValue />}
          />
        </Col>
        <Col xs={4}>
          <KeyValue
            label={<FormattedMessage id="ui-ill-directory.information.mainEmailAddress" />}
            value={emailAddress ?? <NoValue />}
          />
        </Col>
        <Col xs={4}>
          <KeyValue
            label={<FormattedMessage id="ui-ill-directory.information.mainContactName" />}
            value={contactName ?? <NoValue />}
          />
        </Col>
      </Row>
      {addressArray}
    </Accordion>
  );
}

ContactInformation.propTypes = {
  record: PropTypes.object,
  id: PropTypes.string,
  onToggle: PropTypes.func,
  open: PropTypes.bool,
};

export default ContactInformation;
