import { useMemo } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

import {
  Accordion,
  Col,
  KeyValue,
  NoValue,
  Row
} from '@folio/stripes/components';

const DirectoryEntryInfo = ({
  id,
  onToggle,
  open,
  record
}) => {
  const unitList = useMemo(() => {
    if (!record.units || record.units.length === 0) return null;

    return (
      <ul>
        {record.units.map(e => (
          <li key={e.id}><Link to={e.id}>{e.name}</Link></li>
        ))}
      </ul>
    );
  }, [record.units]);

  const memberList = useMemo(() => {
    if (!record.members || record.members.length === 0) return null;

  return (
    <ul>
      {record.members.map(e => (
        <li key={e.id}><Link to={e.id}>{e.name}</Link></li>
      ))}
    </ul>
  );
  }, [record.members]);

  return (
    <Accordion
      id={id}
      label={<FormattedMessage id="ui-ill-directory.information.heading.directoryEntry" />}
      open={open}
      onToggle={onToggle}
    >
      <Row>
        <Col xs={4}>
          <KeyValue
            label={<FormattedMessage id="ui-ill-directory.information.name" />}
            value={record.name}
          />
        </Col>
        <Col xs={4}>
          <KeyValue
            label={<FormattedMessage id="ui-ill-directory.information.type" />}
            value={record.type?.label || <NoValue />}
          />
        </Col>
        <Col xs={4}>
          <KeyValue
            label={<FormattedMessage id="ui-ill-directory.information.slug" />}
            value={record.slug}
          />
        </Col>
      </Row>

      {!record.description ? '' :
      <Row>
        <Col xs={12}>
          <KeyValue
            label={<FormattedMessage id="ui-ill-directory.information.description" />}
            value={record.description}
          />
        </Col>
      </Row>
      }

      {!record.symbolSummary ? '' :
      <Row>
        <Col xs={8}>
          <KeyValue
            label={<FormattedMessage id="ui-ill-directory.information.symbols" />}
            value={record.symbolSummary}
          />
        </Col>
        {!record.lmsLocationCode ? '' :
        <Col xs={4}>
          <KeyValue
            label={<FormattedMessage id="ui-ill-directory.information.lmsLocationCode" />}
            value={record.lmsLocationCode}
          />
        </Col>
        }
      </Row>
      }

      {record.fullyQualifiedName === record.name ? '' :
      <>
        <Row>
          <Col xs={12}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-directory.information.qualifiedName" />}
              value={record.fullyQualifiedName}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={12}>
            <Link to={record.parent.id}>
              <KeyValue
                label={<FormattedMessage id="ui-ill-directory.information.parent" />}
                value={record.parent.name}
              />
            </Link>
          </Col>
        </Row>
      </>
      }

      {!unitList ? '' :
      <>
        <Row>
          <Col xs={12}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-directory.information.units" />}
              value={unitList}
            />
          </Col>
        </Row>
      </>
      }

      {!memberList ? '' :
      <>
        <Row>
          <Col xs={12}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-directory.information.members" />}
              value={memberList}
            />
          </Col>
        </Row>
      </>
      }

      <Row>
        <Col xs={12}>
          <KeyValue
            label={<FormattedMessage id="ui-ill-directory.information.tags" />}
            value={record.tagSummary}
          />
        </Col>
      </Row>
    </Accordion>
  );
};

DirectoryEntryInfo.propTypes = {
  record: PropTypes.object,
  id: PropTypes.string,
  onToggle: PropTypes.func,
  open: PropTypes.bool,
};

export default DirectoryEntryInfo;
