import { useMemo } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Card } from '@folio/stripes/components';

import css from './Address.css';

const Address = ({
  address,
  count,
  index
}) => {
  const sortBySeq = (lineA, lineB) => {
    return lineA.seq - lineB.seq;
  };

  const renderAddress = (address) => {
    address.lines.sort((a, b) => sortBySeq(a, b));
    return (
      <ul className={css.addressList}>
        {address.lines.map((line) => {
          return (
            <li key={`addressLine[${line.seq}]`}>{line.value}</li>
          );
        })}
      </ul>
    );
  };

  const header = useMemo(() => address.addressLabel ||
    <FormattedMessage id="ui-ill-directory.information.addressNofM" values={{ index, count }} />, [address, index, count]);

    return (
      <>
        <Card
          headerStart={header}
        >
          {renderAddress(address)}
        </Card>
      </>
    );
};

Address.propTypes = {
  address: PropTypes.shape({
    lines: PropTypes.arrayOf(PropTypes.shape({
      value: PropTypes.string,
    })),
  }),
  count: PropTypes.number,
  index: PropTypes.number,
};

export default Address;
