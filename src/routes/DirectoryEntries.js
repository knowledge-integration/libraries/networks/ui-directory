import { useCallback, useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';

import { v4 as uuidv4 } from 'uuid';

import get from 'lodash/get';
import { FormattedMessage } from 'react-intl';
import compose from 'compose-function';

import {
  MultiSelectionFilter,
  SearchAndSort,
  withTags
} from '@folio/stripes/smart-components';

import {
  Accordion,
  FilterAccordionHeader
} from '@folio/stripes/components';

import {
  CUSTOM_PROPERTIES_ENDPOINT,
  DIRECTORY_ENTRIES_ENDPOINT,
} from '@k-int/ill-ui';

import { stripesConnect, useOkapiKy } from '@folio/stripes/core';

import ViewDirectoryEntry from '../components/ViewDirectoryEntry';
import EditDirectoryEntry from '../components/EditDirectoryEntry';
import packageInfo from '../../package';

import { parseFilters, deparseFilters } from '../util/parseFilters';
import {
  BASE_DIRECTORY_ENTRY_QUERY_KEY,
  useDirectoryEntry,
  useDirectoryTags,
  useMutateDirectoryEntries
} from "../hooks";
import { useQuery } from "react-query";
import { generateKiwtQueryParams } from "@k-int/stripes-kint-components";
import { DirectoryEntryTags } from '../components/helpers';
import { useILLRefdata } from '@k-int/stripes-ill';

const INITIAL_RESULT_COUNT = 100;

const searchableIndexes = [
  { label: 'Search all fields', value: 'name,tags.value,symbols.symbol' },
  { label: 'Name', value: 'name' },
  { label: 'Tags', value: 'tags.value' },
  { label: 'Symbols', value: 'symbols.symbol' },
];

const appDetails = {
  directory: {
    title: 'Directory',
    visibleColumns: [
      'fullyQualifiedName',
      'type',
      'tagSummary',
      'symbolSummary'
    ],
  },
};

const DirectoryEntries = ({
  history,
  location,
  mutator,
  resources,
  stripes
}) => {
  const [selectedRecordId, setSelectedRecordId] = useState('');
  const ky = useOkapiKy();

  // FIXME This is horrible and hacky -- here til we can SASQ this thing
  useEffect(() => {
    const id = location?.pathname?.match(/\/([a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12})$/)?.[1];
    if (id && id !== selectedRecordId) {
      setSelectedRecordId(id);
    }
  }, [location]);

  // FIXME there is a pattern of useOkapiQuery in ILL right now...
  // I'd rather we stick to the more standard and modular useQuery and useMutation base,
  // extending to separate hooks where needed.
  const {
    post: createDirectoryEntry,
    put: editDirectoryEntry,
  } = useMutateDirectoryEntries();

  // Hooked up in a shaky way to SAS... refactor please for the love of god when we swap to SASQROute
  const direntQueryParams = useMemo(
    () => generateKiwtQueryParams(
      {
        filterKeys: {
          type: 'type.value',
          tags: 'tags.value',
        },
        sortKeys: {
          type: 'type.label',
          tags: 'tags.value',
          symbols: 'symbols.symbol',
        },
        perPage: 100,
      },
      resources?.query ?? {}
    ),
    [resources.query]
  );

  // TODO Also probs make this a component hook
  const { data: direntData = [], isLoading, isFetching } = useQuery(
    [...BASE_DIRECTORY_ENTRY_QUERY_KEY, direntQueryParams, 'getAll'],
    () => ky.get(`${DIRECTORY_ENTRIES_ENDPOINT}?${direntQueryParams.join('&')}`).json()
  );

  const {
    data: selectedDirent,
    isLoading: isSelectedDirentLoading,
    isFetching: isSelectedDirentFetching
  } = useDirectoryEntry({ id: selectedRecordId });

  // FIXME I don't like that this is now part of our REQUIRED API for Dirents... Talk to Chas
  const handleCreate = useCallback((data) => {
    if (!data?.id) {
      data.id = uuidv4();
    }

    return createDirectoryEntry(data);
  }, []);

  const { data: tags = [] } = useDirectoryTags();
  const tagValues = useMemo(() => tags.map(obj => ({ value: obj.normValue, label: obj.value })), [tags]);

  // I'd like to swap over to newer qindex handling like in OA/Agreements
  const onChangeIndex = useCallback((e) => {
    const qindex = e.target.value;
    stripes.logger.log('action', `changed query-index to '${qindex}'`);
    mutator.query.update({ qindex });
  }, [stripes, mutator]);

  const renderFiltersFromData = useCallback((options) => {
    const byName = parseFilters(resources.query?.filters);

    const values = {
      tags: byName.tags || [],
      type: byName.type || [],
    };

    const setFilterState = (group) => {
      if (group.values === null) {
        delete byName[group.name];
      } else {
        byName[group.name] = group.values;
      }
      mutator.query.update({ filters: deparseFilters(byName) });
    };
    const clearGroup = (name) => setFilterState({ name, values: [] });

    const renderGenericFilterSelection = (filterName) => {
      return (
        <Accordion
          label={<FormattedMessage id={`ui-ill-directory.filter.${filterName}`} />}
          id={filterName}
          name={filterName}
          separator={false}
          header={FilterAccordionHeader}
          displayClearButton={values[filterName].length > 0}
          onClearFilter={() => clearGroup(filterName)}
        >
          <MultiSelectionFilter
            name={filterName}
            dataOptions={options[filterName]}
            selectedValues={values[filterName]}
            onChange={setFilterState}
          />
        </Accordion>
      );
    };

    return (
      <>
        {renderGenericFilterSelection('type')}
        {renderGenericFilterSelection('tags')}
      </>
    );
  }, [mutator, resources.query]);

  const type = useILLRefdata('DirectoryEntry.Type');

  const helperApps = useMemo(() => ({ tags: DirectoryEntryTags }), []);

  // FIXME I hate this with vigorous passion
  const constructedPackageInfo = useMemo(() => {
    const path = `/${DIRECTORY_ENTRIES_ENDPOINT}`;

    return {
      ...packageInfo,
      stripes: {
        ...packageInfo.stripes,
        route: path,
        home: path
      }
    }
  }, [packageInfo]);

  return (
    <SearchAndSort
      key="dirents"
      objectName="dirents"
      packageInfo={constructedPackageInfo}
      searchableIndexes={searchableIndexes}
      selectedIndex={get(resources.query, 'qindex')}
      onChangeIndex={onChangeIndex}
      initialResultCount={INITIAL_RESULT_COUNT}
      resultCountIncrement={INITIAL_RESULT_COUNT}
      getHelperComponent={(name) => helperApps[name]}
      getHelperResourcePath={(helper, id) => `${DIRECTORY_ENTRIES_ENDPOINT}/${id}`}
      viewRecordComponent={ViewDirectoryEntry}
      editRecordComponent={EditDirectoryEntry}
      viewRecordPerms="module.ill-directory.enabled"
      newRecordPerms="ui-ill-directory.create"
      onCreate={handleCreate}
      // Manually overwrite select row here because stripes object is being weird
      onSelectRow={(e, row) => {
        history.push(`/ill-directory/entries/view/${row.id}${location.search}`);
      }}
      detailProps={{
        onCreate: handleCreate,
        onUpdate: editDirectoryEntry
      }}
      parentResources={{
        ...resources,
        // FIXME I hate this, cheating the SAS resource shape -- replace with SASQ when we get a chance
        selectedRecord: {
          ...selectedDirent,
          hasLoaded: !isSelectedDirentLoading,
          isPending: isSelectedDirentFetching,
          records: [selectedDirent],
        },
        records: {
          hasLoaded: !isLoading,
          isPending: isFetching,
          records: direntData?.results,
          other: {
            totalRecords: direntData.totalRecords,
          }
        },
        custprops: get(resources, 'custprops.records', []),
      }}
      parentMutator={{
        query: mutator.query,
        resultCount: mutator.resultCount,
      }}
      showSingleResult
      visibleColumns={appDetails.directory.visibleColumns}
      columnMapping={{
        fullyQualifiedName: <FormattedMessage id="ui-ill-directory.entries.name" />,
        type: <FormattedMessage id="ui-ill-directory.entries.type" />,
        tagSummary: <FormattedMessage id="ui-ill-directory.entries.tagSummary" />,
        symbolSummary: <FormattedMessage id="ui-ill-directory.entries.symbolSummary" />,
      }}
      columnWidths={{
        fullyQualifiedName: '40%',
        type: '20%',
        tagSummary: '20%',
        symbolSummary: '20%',
      }}
      resultsFormatter={{
        type: a => a.type?.label || '',
        tagSummary: a => a.tagSummary || '',
        symbolSummary: a => a.symbolSummary || '',
      }}
      renderFilters={() => renderFiltersFromData({
        tags: tagValues,
        type
      })}
    />
  );
};

DirectoryEntries.manifest = Object.freeze({
  custprops: {
    type: 'okapi',
    path: CUSTOM_PROPERTIES_ENDPOINT,
    params: { perPage: '100' },
    shouldRefresh: () => false,
  },
  resultCount: { initialValue: INITIAL_RESULT_COUNT },
  // If this (query) isn't here, then we get this.props.parentMutator.query is undefined in the UI
  query: {},

  selectedRecordId: { initialValue: '' },
});

DirectoryEntries.propTypes = {
  resources: PropTypes.shape({
    query: PropTypes.shape({
      qindex: PropTypes.string,
    }),
    custprops: PropTypes.object,
  }),

  mutator: PropTypes.object,

  stripes: PropTypes.shape({
    logger: PropTypes.shape({
      log: PropTypes.func,
    }),
  }),
};

export default stripesConnect(DirectoryEntries);
