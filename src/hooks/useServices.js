import { useQuery } from 'react-query';
import { useOkapiKy } from '@folio/stripes/core';
import { DIRECTORY_SERVICE_ENDPOINT } from '@k-int/ill-ui';

import { generateKiwtQueryParams } from '@k-int/stripes-kint-components';

// EXAMPLE -- I'd like to slowly refactor away from useOkapiQuery where possible,
// I don't like the "does everything" pattern when react query is already so modular.
const useServices = ({
  paramMap: passedParamMap = {},
  queryOptions = {},
  returnQueryObject = false
} = {}) => {
  const ky = useOkapiKy();

  const paramMap = {
    filters: [
      {
        path: 'status.value',
        value: 'managed'
      }
    ],
    pageSize: 100,
    sort: { path: 'id' },
    ...passedParamMap
  };
  const queryParams = generateKiwtQueryParams(paramMap, {});

  const servicesQueryOutput = useQuery(
    ['ILL', 'Directory', 'Services', queryParams, DIRECTORY_SERVICE_ENDPOINT],
    () => ky.get(`${DIRECTORY_SERVICE_ENDPOINT}?${queryParams.join('&')}`).json(),
    queryOptions
  );

  if (returnQueryObject) {
    return servicesQueryOutput
  }

  return servicesQueryOutput.data?.results ?? [];
};

export default useServices;
