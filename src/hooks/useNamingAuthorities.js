import { useQuery } from 'react-query';
import { useOkapiKy } from '@folio/stripes/core';
import { DIRECTORY_NAMING_AUTHORITY_ENDPOINT } from '@k-int/ill-ui';

import { generateKiwtQueryParams } from '@k-int/stripes-kint-components';

// EXAMPLE -- I'd like to slowly refactor away from useOkapiQuery where possible,
// I don't like the "does everything" pattern when react query is already so modular.
const useNamingAuthorities = ({
  paramMap: passedParamMap = {},
  queryOptions = {},
  returnQueryObject = false
} = {}) => {
  const ky = useOkapiKy();

  const paramMap = { pageSize: 100, ...passedParamMap };
  const queryParams = generateKiwtQueryParams(paramMap, {});

  const namingAuthoritiesQueryOutput = useQuery(
    ['ILL', 'Directory', 'NamingAuthorities', queryParams, DIRECTORY_NAMING_AUTHORITY_ENDPOINT],
    () => ky.get(`${DIRECTORY_NAMING_AUTHORITY_ENDPOINT}?${queryParams.join('&')}`).json(),
    queryOptions
  );

  if (returnQueryObject) {
    return namingAuthoritiesQueryOutput
  }

  return namingAuthoritiesQueryOutput.data?.results ?? [];
};

export default useNamingAuthorities;
