import { useQuery } from 'react-query';

import { useOkapiKy } from '@folio/stripes/core';
import { generateKiwtQueryParams } from '@k-int/stripes-kint-components';

import { TAGS_ENDPOINT } from '@k-int/ill-ui';

const useDirectoryTags = ({
  queryOptions = {},
  queryParams = {}
} = {}) => {
  const ky = useOkapiKy();
  const tagsQueryParams = generateKiwtQueryParams({
    perPage: 100,
    stats: false,
    ...queryParams
  }, {})

  // Get internal DirectoryEntryTags
  return useQuery(
    ['ILL', 'Directory', 'Tags', tagsQueryParams],
    () => ky.get(`${TAGS_ENDPOINT}?${tagsQueryParams?.join('&')}`).json(),
    {
      ...queryOptions
    }
  );
};

export default useDirectoryTags;
