const BASE_DIRECTORY_ENTRY_QUERY_KEY = ['ILL', 'DirectoryEntry'];

const getDirectoryEntryQueryKey = ({
    queryKeys = [],
}) => {
    return [...BASE_DIRECTORY_ENTRY_QUERY_KEY, ...queryKeys];
};

export {
    BASE_DIRECTORY_ENTRY_QUERY_KEY,
    getDirectoryEntryQueryKey,
};
