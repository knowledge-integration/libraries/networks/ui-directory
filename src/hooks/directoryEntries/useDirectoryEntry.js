import { useQuery } from 'react-query';
import { useOkapiKy } from '@folio/stripes/core';
import { DIRECTORY_ENTRIES_ENDPOINT } from '@k-int/ill-ui';

import { BASE_DIRECTORY_ENTRY_QUERY_KEY } from './DirectoryEntryQueryKey';


const useDirectoryEntry = ({ id }) => {
  const ky = useOkapiKy();
  return useQuery(
    [...BASE_DIRECTORY_ENTRY_QUERY_KEY, id, 'get'],
    () => ky.get(`${DIRECTORY_ENTRIES_ENDPOINT}/${id}`).json(),
    {
      enabled: !!id
    }
  );
};

export default useDirectoryEntry;

