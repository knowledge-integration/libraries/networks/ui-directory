import { useQueryClient } from 'react-query';
import { BASE_DIRECTORY_ENTRY_QUERY_KEY } from './DirectoryEntryQueryKey';

const useInvalidateDirectoryEntries = () => {
  const queryClient = useQueryClient();
  return () => queryClient.invalidateQueries(BASE_DIRECTORY_ENTRY_QUERY_KEY);
};

export default useInvalidateDirectoryEntries;
