import { FormattedMessage } from "react-intl";
import { useHistory } from 'react-router-dom';

import noop from 'lodash/noop';

import { useCallout } from "@folio/stripes/core";
import { parseErrorResponse, useMutateGeneric } from "@k-int/stripes-kint-components";
import {
  BASE_DIRECTORY_ENTRIES_URL,
  DIRECTORY_ENTRIES_ENDPOINT,
  DIRECTORY_ENTRIES_VIEW_URL,
} from "@k-int/ill-ui";

import { BASE_DIRECTORY_ENTRY_QUERY_KEY } from "./DirectoryEntryQueryKey";
import { useInvalidateDirectoryEntries } from './index';

// TODO This seems like a useful pattern to generalise instead of copying
const useMutateDirectoryEntries = ({
    afterQueryCalls: {
        delete: afterQueryDelete = noop,
        post: afterQueryPost = noop,
        put: afterQueryPut = noop,
    } = {},
    catchQueryCalls: {
        delete: catchQueryDelete = noop,
        post: catchQueryPost = noop,
        put: catchQueryPut = noop,
    } = {},
    sendSuccessCallouts: {
        delete: sendSuccessCalloutDelete = true,
        post: sendSuccessCalloutPost = true,
        put: sendSuccessCalloutPut = true,
    } = {},
    sendErrorCallouts: {
        delete: sendErrorCalloutDelete = true,
        post: sendErrorCalloutPost = true,
        put: sendErrorCalloutPut = true,
    } = {},
    redirect: {
        delete: redirectDelete = true,
        post: redirectPost = true,
        put: redirectPut = true,
    } = {},
    ...mutateDirectoryEntryProps
} = {}) => {
    const callout = useCallout();

    const history = useHistory();
    const invalidateDirectoryEntries = useInvalidateDirectoryEntries();

    // TODO the neutering of location.search is down to the stupid SAS insistence on "layer" for create rather than routing.
    // Once we have moved to SASQ we should add this back in so that routing remembers search queries etc
    return useMutateGeneric({
        afterQueryCalls: {
            delete: (res) => {
                invalidateDirectoryEntries()
                    .then(() => {
                        if (sendSuccessCalloutDelete) {
                            callout.sendCallout({message: <FormattedMessage id="ui-ill-directory.delete.callout"/>});
                        }

                        if (redirectDelete) {
                            // history.push(`${DIRECTORY_ENTRIES_VIEW_URL_BASE}${location.search}`);
                            history.push(BASE_DIRECTORY_ENTRIES_URL);
                        }

                        afterQueryDelete(res)
                    });
            },
            post: (res) => {
                invalidateDirectoryEntries()
                    .then(() => {
                        if (sendSuccessCalloutPost) {
                            callout.sendCallout({
                                message: <FormattedMessage id="ui-ill-directory.create.callout"
                                                           values={{name: res.name}}/>
                            });
                        }

                        if (redirectPost) {
                          // history.push(`${DIRECTORY_ENTRIES_VIEW_URL(res.id)}${location.search}`);
                          history.push(DIRECTORY_ENTRIES_VIEW_URL(res.id));
                        }

                        afterQueryPost(res);
                    });
            },
            put: (res) => {
                invalidateDirectoryEntries()
                    .then(() => {
                        if (sendSuccessCalloutPut) {
                            callout.sendCallout({
                                message: <FormattedMessage id="ui-ill-directory.edit.callout"
                                                           values={{name: res.name}}/>
                            });
                        }

                        if (redirectPut) {
                            // history.push(`${DIRECTORY_ENTRIES_VIEW_URL(res.id)}${location.search}`);
                            history.push(DIRECTORY_ENTRIES_VIEW_URL(res.id));
                        }

                        afterQueryPut(res);
                    });
            },
        },
        catchQueryCalls: {
            delete: (res) => {
                parseErrorResponse(res.response)
                    .then((error) => {
                        if (sendErrorCalloutDelete) {
                            callout.sendCallout({ type: 'error', message: <FormattedMessage id="ui-ill-directory.delete.callout.error" values={{ err: error.message }} /> });
                        }
                        // Pass down original response object
                        catchQueryDelete(res);
                    })
                    .catch(() => {
                        if (sendErrorCalloutDelete) {
                            callout.sendCallout({ type: 'error', message: <FormattedMessage id="ui-ill-directory.delete.callout.error" values={{ err: '' }} /> })
                        }
                    });
            },
            post: (res) => {
                parseErrorResponse(res.response)
                    .then((error) => {
                        if (sendErrorCalloutPost) {
                            callout.sendCallout({ type: 'error', message: <FormattedMessage id="ui-ill-directory.create.callout.error" values={{ err: error.message }} /> });
                        }
                        // Pass down original response object
                        catchQueryPost(res);
                    })
                    .catch(() => {
                        if (sendErrorCalloutPost) {
                            callout.sendCallout({ type: 'error', message: <FormattedMessage id="ui-ill-directory.create.callout.error" values={{ err: '' }} /> })
                        }
                    });
            },
            put: (res) => {
                parseErrorResponse(res.response)
                    .then((error) => {
                        if (sendErrorCalloutPut) {
                            callout.sendCallout({ type: 'error', message: <FormattedMessage id="ui-ill-directory.update.callout.error" values={{ err: error.message }} /> });
                        }
                        // Pass down original response object
                        catchQueryPut(res);
                    })
                    .catch(() => {
                        if (sendErrorCalloutPut) {
                            callout.sendCallout({ type: 'error', message: <FormattedMessage id="ui-ill-directory.update.callout.error" values={{ err: error.message }} /> });
                        }
                    });
            },
        },
        endpoint: DIRECTORY_ENTRIES_ENDPOINT,
        queryKey: BASE_DIRECTORY_ENTRY_QUERY_KEY,
        ...mutateDirectoryEntryProps
    });
};

export default useMutateDirectoryEntries;
