## [1.9.4](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/compare/v1.9.3...v1.9.4) (2023-11-04)


### Bug Fixes

* **maybe:** Directory no longer acts as a settings app ([e1c973a](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/e1c973a2156b6d0292a35c9474c98a4fb3faa391))

## [1.9.3](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/compare/v1.9.2...v1.9.3) (2023-11-03)


### Bug Fixes

* **permissions:** Add missing settings permissions ([216b32e](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/216b32e358d17b8f4f1963d83ac62aa58619b771))

## [1.9.2](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/compare/v1.9.1...v1.9.2) (2023-10-25)


### Bug Fixes

* FieldArrays use fields.name not input.name ([4bc40ac](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/4bc40accd1290668e30cd88abd01126935000bc9))

## [1.9.1](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/compare/v1.9.0...v1.9.1) (2023-10-22)


### Bug Fixes

* Error in top level component ([ad3b62d](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/ad3b62d95552e64047af48880806cfbd96c2bae5))

# [1.9.0](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/compare/v1.8.0...v1.9.0) (2023-10-21)


### Features

* **i18n:** Replicate i18n approach from agreements ([c5492fa](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/c5492fa3e4018b70fead4e443ef363df15745afa))

# [1.8.0](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/compare/v1.7.3...v1.8.0) (2023-10-18)


### Bug Fixes

* **build:** Add build-module-descriptor script to package.json ([26596a9](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/26596a9cb49a582f7608fe8023347ce842886936))
* **build:** Don't do stripes semantic-release ([a462b9c](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/a462b9c5675ebc6fde254bdb51cac01a1de54be9))
* **build:** More attempts to get curl working ([6b3e954](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/6b3e95474f3998c1d05fc7d77705e5b2e6261e94))
* **build:** More attempts to get curl working ([c6f9444](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/c6f9444f48864f51a8c2a5a76f22b3fecd3fcedc))
* **build:** More attempts to get curl working ([8ff8bfc](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/8ff8bfcd16d7cbb6936cf39165be51fbee608319))
* **build:** Quote strings containing colon in ci file ([94d6746](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/94d6746c13f6e451069da86b606154f439b6818b))
* **build:** Use single quotes in build script to avoid misparse of colon ([ab10fd5](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/ab10fd54e7691bf3cb4093f1dfdb00b1ab5bc567))
* **dependencies:** Module requires mod-ill not mod-directory ([55550d5](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/55550d57326ab3bb855074ce6e4b3bf2d67cd70a))
* **release:** Build module descriptor ([b929bea](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/b929beaac2bfb59cb4fecd6f74043c50272e58b8))


### Features

* **release:** Upload module descriptor to registry ([e3ecaa9](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/e3ecaa9d834a84ddeaac51913d45a5f9df4fc63f))

## [1.7.3](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/compare/v1.7.2...v1.7.3) (2023-10-08)


### Bug Fixes

* **release:** Added publishConfig to package.json ([a80a426](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/a80a4266a335d283bb0d71ea99ad87bd515af927))

## [1.7.2](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/compare/v1.7.1...v1.7.2) (2023-10-08)


### Bug Fixes

* **release:** More tidying of release files ([a996e77](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/a996e7717a63e79c5e2f59262d4b6c98c747446a))

# [1.7.0](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/compare/v1.6.0...v1.7.0) (2023-10-08)


### Bug Fixes

* **release:** Add .releaserc.json ([19c554b](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/19c554b07f8c2c4e006c4dd2c8b8a8d256c8c457))
* **release:** Add missing dev-dependencies for semantic-release ([40231cb](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/40231cbdf2b667786aabbbd0e9a26207824669a1))
* **release:** Add missing semantic-release script ([46e1041](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/46e1041616649c78b8bfa73de3c6b4119328a6d3))
* **release:** added .gitlab-ci.yml ([43557dc](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/43557dc0a7ff9d06f87b6f0e74d0c90d98a1c401))
* **release:** Use yarn install rather than npm ([7dee649](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/7dee64941af4cbb786803403a721115ca5a2367e))


### Features

* Confirmation ([2a37a8b](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/2a37a8b82962a9bd772ee71890cdbf4063be4a4e))

# [1.7.0](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/compare/v1.6.0...v1.7.0) (2023-10-08)


### Bug Fixes

* **release:** Add .releaserc.json ([19c554b](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/19c554b07f8c2c4e006c4dd2c8b8a8d256c8c457))
* **release:** Add missing dev-dependencies for semantic-release ([40231cb](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/40231cbdf2b667786aabbbd0e9a26207824669a1))
* **release:** Add missing semantic-release script ([46e1041](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/46e1041616649c78b8bfa73de3c6b4119328a6d3))
* **release:** added .gitlab-ci.yml ([43557dc](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/43557dc0a7ff9d06f87b6f0e74d0c90d98a1c401))
* **release:** Use yarn install rather than npm ([7dee649](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/7dee64941af4cbb786803403a721115ca5a2367e))


### Features

* Confirmation ([2a37a8b](https://gitlab.com/knowledge-integration/libraries/networks/ill-directory/commit/2a37a8b82962a9bd772ee71890cdbf4063be4a4e))

# Change history for ui-directory

## [1.2.0](https://github.com/openlibraryenvironment/ui-directory/tree/v1.2.0) (IN PROGRESS)

* Hide "New" and "Add unit" buttons from Directory if user does not have `ui-ill-directory.create` permission. Fixes PR-834.
* Support `ui-ill-directory.edit-local` permission ("edit the local fields of directory entries"). The *Edit* button is shown for all records if the user has this permission, but on the edit page only the *Local information* tab is available when editing a non-managed record if the user does not also have either `ui-ill-directory.edit-all` or `ui-ill-directory.edit-local`. Fixes PR-835.

## [1.1.0](https://github.com/openlibraryenvironment/ui-directory/tree/v1.1.0) (2020-08-24)

* Initial release

## 0.1.0 (NEVER RELEASED)

* New app created with stripes-cli
