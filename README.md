# ui-directory

This module is now released using semantic-release. Please tag commits using semantic markup and release by merging main onto the release branch. The .gitlab-ci.yml
will take care of the release process

Copyright (C) 2018 Knowledge Integration Ltd.

